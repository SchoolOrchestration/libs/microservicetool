# microservicetool

![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)

> Various useful re-usable tools for School-style microservices

## Usage

```
pip install microservicetools
```

## Run the tests

```
docker-compose run --rm web python manage.py test
```

## Pushing an update release:

To push an update to pip simply bump the version and then push to `origin master`: (you may need to `pip install bumpversion`)

```
bumpversion {major|minor|patch}
git push origin master
```

cp ../