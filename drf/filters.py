from rest_framework.filters import BaseFilterBackend

class IdsInFilter(BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    example: ids=1,2,3
    """
    def filter_queryset(self, request, queryset, view):
        ids_string = request.GET.get('ids')
        if ids_string is not None:
            ids = ids_string.split(',')
            if len(ids) > 0:
                queryset = queryset.filter(id__in=ids)
        return queryset